<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('nama_lengkap', 100);
            $table->char('posisi_yang_dilamar', 100);
            $table->char('tempat_lahir', 100);
            $table->dateTime('tanggal_lahir');
            $table->char('no_ktp', 100);
            $table->char('jenis_kelamin', 100);
            $table->char('status_perkawinan', 100);
            $table->char('agama', 100);
            $table->char('kebangsaan', 100);
            $table->dateTime('tanggal_join');
            $table->char('npwp', 100);
            $table->char('domisili', 100);
            $table->char('alamat_ktp', 100);
            $table->char('telepon', 100);
            $table->char('email', 100);
            $table->char('kendaraan', 100);
            $table->char('laptop', 100);
            $table->integer('gaji_diharap');
            $table->integer('anak_ke');
            $table->integer('saudara');
            $table->char('pencapaian_sudah_dilakukan', 255);
            $table->char('pencapaian_yang_diinginkan', 255);
            $table->mediumText('foto')-> nullable();
            $table->char('status_lamaran', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personals');
    }
}
