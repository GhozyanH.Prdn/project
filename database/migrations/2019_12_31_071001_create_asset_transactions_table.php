<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('kode_peminjaman', 100);
            $table->unsignedBigInteger('id_aset');
            $table->unsignedBigInteger('id_pegawai');
            $table->datetime('tanggal_pinjam');
            $table->datetime('tanggal_kembali')-> nullable();
            $table->timestamps();

            $table->foreign('id_aset')
                  ->references('id')
                  ->on('assets')
                  ->onDelete('cascade');

            $table->foreign('id_pegawai')
                  ->references('id')
                  ->on('educations')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_categorys');
    }
}
