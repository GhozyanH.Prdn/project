<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_personal');
            $table->char('jenis_pendidikan', 100);
            $table->char('nama_sekolah', 100);
            $table->char('jurusan', 100);
            $table->year('tahun_mulai');
            $table->year('tahun_selesai');
            $table->char('kota', 100);
            $table->timestamps();

            $table->foreign('id_personal')
                  ->references('id')
                  ->on('personals')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educations');
    }
}
