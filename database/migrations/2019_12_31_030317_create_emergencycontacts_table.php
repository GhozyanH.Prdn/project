<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmergencycontactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emergencycontacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_personal');
            $table->char('nama', 100);
            $table->char('hubungan', 100);
            $table->char('alamat', 100);
            $table->char('telepon', 15);
            $table->timestamps();

            $table->foreign('id_personal')
                  ->references('id')
                  ->on('personals')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergencycontacts');
    }
}
