<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCutiRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuti_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->DateTime('tanggal_request');
            $table->unsignedBigInteger('id_karyawan');
            $table->char('nama_atasan',100);
            $table->unsignedBigInteger('id_cuti');
            $table->DateTime('tanggal_cuti');
            $table->Integer('lama_cuti');
            $table->text('alasan');
            $table->Integer('cuti_terpakai')->nullable();
            $table->Char('keputusan_direktur',50)->nullable();
            $table->Text('catatan_direktur')->nullable();
            $table->Char('keputusan_hrd',50)->nullable();
            $table->Text('catatan_hrd')->nullable();
            $table->Char('keputusan_atasan',50)->nullable();
            $table->Text('catatan_atasan')->nullable();
            $table->timestamps();

            $table->foreign('id_karyawan')
            ->references('id')
            ->on('employees')
            ->onDelete('cascade');

            $table->foreign('id_cuti')
            ->references('id')
            ->on('cuti_categorys')
            ->onDelete('cascade');

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuti_requests');
    }
}
