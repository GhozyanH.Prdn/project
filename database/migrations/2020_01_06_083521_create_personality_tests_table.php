<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalityTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personality_tests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('characterG');
            $table->Integer('characterL');
            $table->Integer('characterI');
            $table->Integer('characterT');
            $table->Integer('characterV');
            $table->Integer('characterR');
            $table->Integer('characterD');
            $table->Integer('characterC');
            $table->Integer('characterE');
            $table->Integer('characterW');
            $table->Integer('characterF');
            $table->Integer('characterK');
            $table->Integer('characterZ');
            $table->Integer('characterO');
            $table->Integer('characterB');
            $table->Integer('characterX');
            $table->Integer('characterP');
            $table->Integer('characterA');
            $table->Integer('characterN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personality_tests');
    }
}
