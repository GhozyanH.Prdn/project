<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TrainingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trainings')->insert([
            [
            'id_personal' => '1',
            'jenis_pelatihan' => 'Heru',
            'nama_penyelenggara' => 'PT Indah Kusuma',
            'tanggal_mulai' => '2019-12-26 00:00:00',
            'tanggal_selesai' => '2019-12-26 00:00:00',
            'tempat' => 'Web Programmer',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],
            [
            'id_personal' => '1',
            'jenis_pelatihan' => 'Heru',
            'nama_penyelenggara' => 'PT Indah Kusuma',
            'tanggal_mulai' => '2019-12-26 00:00:00',
            'tanggal_selesai' => '2019-12-26 00:00:00',
            'tempat' => 'Web Programmer',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
                ],
        ]);
    }
}
