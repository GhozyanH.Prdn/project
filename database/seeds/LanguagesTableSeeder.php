<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            [
            'id_personal' => '1',
            'language' => 'spanish',
            'oral' => 'Kepala Gugus',
            'written' => '2000',
            'reading' => '2000',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],
            [
            'id_personal' => '1',
            'language' => 'spanish',
            'oral' => 'Kepala Gugus',
            'written' => '2000',
            'reading' => '2000',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],

        ]);
    }
}
