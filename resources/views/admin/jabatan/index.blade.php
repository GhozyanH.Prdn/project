@extends('layouts.master')

@section('content')
          <!-- Breadcome start-->
          <div class="breadcome-area mg-b-30 des-none">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                          <div class="row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <div class="breadcome-heading">
                                      <form role="search" class="">
                  <input type="text" placeholder="Search..." class="form-control">
                  <a href=""><i class="fa fa-search"></i></a>
                </form>
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <ul class="breadcome-menu">
                                      <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                      </li>
                                      <li><span class="bread-blod">Dashboard</span>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          </div>
          <!-- Breadcome End-->
          <!-- Static Table Start -->
          <div class="data-table-area mg-b-10">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-lg-12">
                          <div class="sparkline13-list shadow-reset">
                              <div class="sparkline13-hd">
                                  <div class="main-sparkline13-hd">
                                      <h1>Projects <span class="table-project-n">Data</span> Table</h1>
                                      <div class="view-mail-action view-mail-ov-d-n">
                                                    <a href="assets/create"><i class="fa fa-reply"></i> Tambah Data</a>

                                                    <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                                    <span><i class="fa fa-wrench"></i></span>
                                                    <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span>
                                      </div>
                                      <div class="sparkline13-outline-icon">

                                      </div>
                                  </div>
                              </div>
                              <div class="sparkline13-graph">
                                  <div class="dt">
                                      <div id="toolbar">
                                          <select class="form-control">
                                              <option value="">Export Basic</option>
                                              <option value="all">Export All</option>
                                              <option value="selected">Export Selected</option>
                                          </select>
                                      </div>
                                      <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                          <thead>
                                              <tr>
                                                  <th data-field="id">No Aset</th>
                                                  <th data-field="name">Kode Aset</th>
                                                  <th data-field="deskripsi">Nama Aset</th>
                                                  <!-- <th data-field="email">Nama Aset</th>
                                                  <th data-field="phone">Tanggal Beli</th>
                                                  <th data-field="company">Gambar</th> -->
                                                  <th data-field="action">Action</th>
                                              </tr>
                                          </thead>
                                          <tbody>

                                            @foreach ($positions as $pst)
                                              <tr>
                                                  <td>{{$loop -> iteration}}</td>
                                                  <td>{{$pst -> kode_jabatan}}</td>
                                                  <td>{{$pst -> nama_jabatan}}</td>
                                                  <!-- <td>{{$pst -> nama_jabatan}}</td>
                                                  <td>{{$pst -> nama_jabatan}}</td>
                                                  <td>{{$pst -> nama_jabatan}}</td> -->
                                                  <td>
                                                  <form action="/assets/{{$pst -> id}}" method="post" class="d-inline">
                                                    @method('delete')
                                                    @csrf
                                                  <a href="#" class="btn btn-custon-four btn-default">Penyerahan</a>
                                                  <a href="/assets/{{$pst->id}}" class="btn btn-custon-four btn-success">Detil</a>
                                                  <a href="/assets/{{$pst->id}}/edit" class="btn btn-custon-four btn-warning">Edit</a>
                                                    <button type="submit" class="btn btn-custon-four btn-danger" >Delete</button>
                                                  </form>
                                                  </td>
                                              </tr>
                                          @endforeach
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                              <!-- Button start-->
<!-- Button End-->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
<!-- Static Table End -->

            @endsection
