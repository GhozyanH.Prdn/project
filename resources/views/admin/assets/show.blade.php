@extends('layouts.master')

@section('content')
<div class="basic-form-area mg-b-15">
    <div class="container-fluid">
      <div class="user-profile-area">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="user-profile-wrap shadow-reset">
                          <div class="row">
                              <div class="col-lg-5">
                                  <div class="row">
                                      <div class="col-lg-3">
                                          <div class="user-profile-img">
                                              <img src="/material/img/notification/5.jpg" alt="" />
                                          </div>
                                      </div>
                                      <div class="col-lg-9">
                                          <div class="user-profile-content">
                                              <h2>{{$asset -> nama_aset}}</h2>
                                              <p class="profile-founder">Kategori&emsp;&emsp;&emsp;&ensp;:&emsp;<strong>{{$asset -> kategori}}</strong>
                                              </p>
                                              <p class="profile-founder">Kode Aset&emsp;&emsp;&ensp;:&emsp;<strong>{{$asset -> kode_aset}}</strong>
                                              </p>
                                              <p class="profile-founder">Tanggal Beli&emsp;&ensp;:&emsp;<strong>{{$asset -> tanggal_beli}}</strong>
                                              </p>
                                              @foreach ($users as $user)
                                              <p class="profile-founder">PIC Sekarang&emsp;:&emsp;<strong>{{$user -> nama}}</strong>
                                                @endforeach
                                              </p>
                                              <p class="profile-des"></p>
                                          </div>

                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-7">
                                  <div class="user-profile-social-list">
                                      <table class="table small m-b-xs">
                                        <thead>
                                            <tr>
                                              <th>No </th>
                                              <th >Kode Peminjaman</th>
                                              <th>Tanggal Peminjaman</th>
                                              <th>Tanggal Kembali</th>
                                              <th>Nama PIC</th>
                                            </tr>
                                        </thead>
                                          <tbody>
                                            @foreach ($tes as $at)
                                              <tr>
                                                <td>{{$loop -> iteration}}</td>
                                                <td>{{$at -> kode_peminjaman}}</td>
                                                <td>{{$at -> tanggal_pinjam}}</td>
                                                <td>{{$at -> tanggal_kembali}}</td>
                                                <td>{{$at -> nama}}</td>

                                              </tr>
                                          @endforeach
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>

<!-- Basic Form Start -->




<!-- Basic Form End-->


            @endsection
