@extends('layouts.master')

@section('content')
<!-- Basic Form Start -->
<div class="basic-form-area mg-b-15">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline12-list shadow-reset mg-t-30">
                    <div class="sparkline12-hd">
                        <div class="main-sparkline12-hd">
                            <h1>Penyerahan Data Aset</h1>
                            <div class="sparkline12-outline-icon">
                                <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                <span><i class="fa fa-wrench"></i></span>
                                <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="sparkline12-graph">
                        <div class="basic-login-form-ad">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="all-form-element-inner">

                                        <form action="" method="post">

                                          @method('patch')
                                          @csrf
                                          <div class="form-group-inner">
                                              <div class="row">
                                                  <div class="col-lg-3">
                                                      <label class="login2 pull-right pull-right-pro">Kode Penyerahan</label>
                                                  </div>
                                                  <div class="col-lg-9">
                                                      <input type="text" disabled class="form-control"  name="kode_penyerahan" placeholder="Automatic Generate" />
                                                  </div>
                                              </div>
                                          </div>

                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Kategori</label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input type="text"  class="form-control"  @foreach ($transaksi as $trr) value="{{$trr -> kategori}}" @endforeach name="kategori" readonly/>
                                                    </div>
                                                </div>
                                              </div>


                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Kode Aset</label>
                                                    </div>
                                                    <div class="col-lg-9">

                                                        <input type="text"  class="form-control" @foreach ($transaksi as $trr) value="{{$trr -> kode_aset}}" @endforeach name="kode_aset"  readonly/>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Nama Aset</label>
                                                    </div>
                                                    <div class="col-lg-9">

                                                        <input type="text" name="nama_aset" @foreach ($transaksi as $trr) value="{{$trr -> nama_aset}}" @endforeach class="form-control" readonly/>

                                                    </div>
                                                </div>
                                              </div>

                                              <div class="form-group-inner">
                                                  <div class="row">
                                                      <div class="col-lg-3">
                                                          <label class="login2 pull-right pull-right-pro">Tanggal Beli</label>
                                                      </div>
                                                      <div class="col-lg-9">

                                                          <input type="text"  name="tanggal_beli" @foreach ($transaksi as $trr) value="{{$trr -> tanggal_beli}}" @endforeach class="form-control" readonly />

                                                      </div>
                                                  </div>
                                                </div>

                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="login2 pull-right pull-right-pro">Foto Aset</label>
                                                        </div>
                                                        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="file-upload-inner file-upload-inner-right ts-forms">
                                                                <div class="input append-small-btn">

                                                                    <input type="text" name="gambar" id="append-small-btn" @foreach ($transaksi as $trr) value="{{$trr -> gambar}}" @endforeach name="gambar" readonly>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Pilih Pegawai</label>
                                                                </div>
                                                                <div class="col-lg-9">
                                                                    <div class="form-select-list">
                                                                        <select class="form-control custom-select-value" name="id_pegawai">

                                                                          @foreach ($employee_array as $data)
                                                                          <option value="{{ $data->id }}" >{{ $data->nama }}</option>
                                                                          @endforeach


                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Tanggal Penyerahan</label>
                                                                </div>
                                                                <div class="col-lg-9">
                                                                    <input type="text"  class="form-control" name="tanggal_pinjam" value="{{ date(' Y-m-d') }}" readonly/>
                                                                </div>
                                                            </div>
                                                        </div>


                                            <div class="form-group-inner">
                                                <div class="login-btn-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3"></div>
                                                        <div class="col-lg-9">
                                                            <div class="login-horizental cancel-wp pull-right">
                                                                <button class="btn btn-custon-rounded-four btn-warning" type="submit">Cancel</button>
                                                                <button class="btn btn-custon-rounded-four btn-success" type="submit">Save Change</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic Form End-->


            @endsection
