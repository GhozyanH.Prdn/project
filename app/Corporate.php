<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Corporate extends Model
{
    protected $table='corporate_groups';

    use SoftDeletes;
    protected $fillable = ['kode_corporate_group', 'nama_corporate_group'];

    public static function getId(){
      return $getId = DB::table('corporate_groups')->orderBy('id','DESC')->take(1)->get();
}
}