<?php

namespace App\Http\Controllers;

use App\Cuti;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CutiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $cuti3 = DB::table('cuti_requests')
        ->select('cuti_requests.id','cuti_requests.tanggal_request','employees.nama','cuti_requests.nama_atasan','cuti_categorys.jenis_cuti','cuti_requests.alasan','cuti_requests.tanggal_cuti','cuti_requests.lama_cuti')
        ->join('employees','cuti_requests.id_karyawan','employees.id')
        ->join('cuti_categorys','cuti_requests.id_cuti','cuti_categorys.id')
        ->get();
        return view ('admin/cuti.index',compact('cuti3'));
        // $cuti3 = Cuti::all();
        // return view ('admin/cuti.index',compact('cuti3'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function show(Cuti $cuti)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function edit(Cuti $cuti)
    {
        $cuti3 = DB::table('cuti_requests')
        ->select('cuti_requests.id','cuti_requests.tanggal_request','employees.nama','cuti_requests.nama_atasan','cuti_categorys.jenis_cuti','cuti_requests.alasan','cuti_requests.tanggal_cuti','cuti_requests.lama_cuti')
        ->join('employees','cuti_requests.id_karyawan','employees.id')
        ->join('cuti_categorys','cuti_requests.id_cuti','cuti_categorys.id')
        ->get();
        
        return view('admin.cuti.edit', compact('cuti','cuti3'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cuti $cuti)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cuti $cuti)
    {
        //
    }
}
