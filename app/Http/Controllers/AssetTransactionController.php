<?php

namespace App\Http\Controllers;

use App\AssetTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AssetTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tes = AssetTransaction::all();
      return view('admin.assets_transactions.penyerahan', compact('tes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(AssetTransaction $AssetTransaction)
    {
      $transaksi = DB::table('asset_transactions')
      ->select('asset_transactions.id','assets.kode_aset','assets.nama_aset','assets.gambar','assets.kategori','assets.tanggal_beli','asset_transactions.id_pegawai','asset_transactions.tanggal_pinjam','employees.nama')
      ->join('assets','asset_transactions.id_aset','assets.id')
      ->join('employees','asset_transactions.id_pegawai','employees.id')
      ->get();

      //dd($transaksi);
        return view('admin.assets_transactions.penyerahan',compact('transaksi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset_Transaction  $asset_Transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Asset_Transaction $asset_Transaction)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset_Transaction  $asset_Transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset_Transaction $asset_Transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset_Transaction  $asset_Transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset_Transaction $asset_Transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset_Transaction  $asset_Transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset_Transaction $asset_Transaction)
    {
        //
    }
}
