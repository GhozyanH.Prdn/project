<?php

namespace App\Http\Controllers;

use App\Grade;
use Illuminate\Http\Request;
use Carbon;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = Grade::all();
        return view ('admin/grade.index',compact('grades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.grade.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Grade::getId();
        foreach ($id as $value);
        $idlama = $value->id;
        $idbaru = $idlama + 1;
        $kode_grade = 'GA'.$idbaru;

        $grd= new Grade;
        $grd-> kode_grade = $kode_grade;
        $grd-> nama_grade = $request -> nama_grade;
        $grd -> save();
        
        return redirect('/grade')-> with('status', 'Data Grade Berhasil di Tambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function show(Grade $grade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function edit(Grade $grade)
    {
        return view('admin.grade.edit', compact('grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Grade $grade)
    {
        Grade::where('id',$grade->id)
        ->update([
          'kode_grade' => $request->kode_grade,
          'nama_grade' => $request->nama_grade,
        ]);
        return redirect('/grade')-> with('edit', 'Data Berhasil di Ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grade $grade)
    {
        Grade::destroy($grade->id);
        return redirect('/grade')-> with('delete', 'Data Berhasil di Hapus');
    }

    public function trash(){
        $grade = Grade::onlyTrashed()->get();
        return view ('admin.grade.trash', ['grade' => $grade]);
       
    }
  
      public function restore($id){
        $grade = grade::onlyTrashed()->where('id', $id);
        $grade->restore();
        return redirect('/grade/trash')-> with('restore', 'Data Aset Berhasil di Restore');
      }
  
      public function deleted_permanent($id){
        $grade = grade::onlyTrashed()->where('id', $id);
        $grade->forceDelete();
  
        return redirect('/grade/trash')-> with('delete', 'Data Aset Berhasil di Delete Permanent');
      }
  
      public function restore_all(){
        $grade = Grade::onlyTrashed();
        $grade->restore();
  
        return redirect('/grade/trash')-> with('restore_all', 'Semua Data Berhasil di Restore');
      }
  
      public function deleted_all(){
        $grade = Grade::onlyTrashed();
        $grade->forceDelete();
  
        return redirect('/grade/trash')-> with('deleted_all', 'Data Aset Berhasil di Delete Semua');
      }

}
